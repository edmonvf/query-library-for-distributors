/*Open quotes for current user. For use with count widget*/

SELECT
  "DocNum",
  "CardCode",
  "CardName",
  "UserSign"
FROM OQUT
WHERE "DocStatus" = 'O' 
AND "UserSign" = $[USER]
