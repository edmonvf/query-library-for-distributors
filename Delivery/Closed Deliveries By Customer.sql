/* Delivery Info by customer code*/

SELECT
  "DocEntry",
  "DocNum",
  "CardCode",
  "DocDate",
  "NumAtCard" AS "Customer Ref"
FROM ODLN
WHERE "DocStatus" = 'C'
AND "CardCode" = [%0]
